module.exports.countWords = (str) => {
  return str.split(' ').length;
}