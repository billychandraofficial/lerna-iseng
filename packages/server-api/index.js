const express = require("express");
const stringUtil = require("string-util");
const cors = require("cors");
const port = 11000;
const app = express();

app.use(cors());

app.get('/lorem', (req, res) => {
  res.set('Content-Type', 'application/json');
  res.send({ sentence: stringUtil.generateSentence() });
})

app.listen(port, (err) => {
  if (err) {
    console.error(`Error: ${err.message}`)
  } else {
    console.log(`Listening on port ${port}`)
  }
})