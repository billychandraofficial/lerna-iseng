import logo from './logo.svg';
import axios from 'axios';
import './App.css';
import { useEffect, useState } from 'react';

function App() {
  const [lorem, setLorem] = useState();

  useEffect(() => {
    axios.get('http://localhost:11000/lorem').then(res => {
      console.log(res);
      setLorem(res.data.sentence);
    })
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Here is a lorem ipsum for you: {lorem}
        </p>
      </header>
    </div>
  );
}

export default App;
